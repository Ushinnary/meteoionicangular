import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { Injectable } from '@angular/core';
import { Geolocation } from '@ionic-native/geolocation';
import 'rxjs/add/observable/forkJoin'
@Injectable()
export class WeatherService {
    APPID;
    myGeo: WeatherClass;
    myFavoris: WeatherClass[];
    getByLatLon(lat, lon): Observable<WeatherClass> {
        return this.http.get<WeatherClass>(`http://api.openweathermap.org/data/2.5/forecast?lat=${lat}&lon=${lon}&APPID=${this.APPID}&lang=fr&units=metric`);
    }
    getByCity(city: string): Observable<WeatherClass> {
        const country = city.split(',')[1];
        return country ?
            this.http.get<WeatherClass>(`http://api.openweathermap.org/data/2.5/forecast?q=${city},${country}&APPID=${this.APPID}&lang=fr&units=metric`) :
            this.http.get<WeatherClass>(`http://api.openweathermap.org/data/2.5/forecast?q=${city}&APPID=${this.APPID}&lang=fr&units=metric`);
    }
    getMyGeo(): WeatherClass {
        this.geolocation.getCurrentPosition().then((resp) => {
            this.getByLatLon(
                resp.coords.latitude,
                resp.coords.longitude
            ).subscribe(val => {
                this.setMyGeo(val);
            });
        }).catch((error) => {
            console.log('Error getting location', error);
        });
        return JSON.parse(localStorage.getItem('geo')) || null;
    }
    setMyGeo(weather: WeatherClass): void {
        localStorage.setItem('geo', JSON.stringify(weather));
    }
    getMyFavorisList(): WeatherClass[] {
        return JSON.parse(localStorage.getItem('favoris')) || [];
    }
    updateMyGeo() {
        if (this.myGeo) {
            this.getByCity(this.myGeo.city.name).subscribe(val => {
                this.setMyGeo(val);
                this.myGeo = val;
            });
        }
    }
    updateMyFavoris() {
        if (this.myFavoris) {
            Observable.forkJoin(...this.myFavoris.map(ville => this.getByCity(ville.city.name))).subscribe(val => {
                this.setMyFavorisList(val);
                this.myFavoris = val;
            });
        }
    }
    setMyFavorisList(arr: WeatherClass[]): void {
        localStorage.setItem('favoris', JSON.stringify(arr));
    }
    favorisListChange(city: WeatherClass, add?: boolean) {
        add ? this.myFavoris.push(city) : this.myFavoris.splice(this.myFavoris.indexOf(city), 1);
        localStorage.setItem('favoris', JSON.stringify(this.myFavoris));
    }
    getBackground(weather: string): string {
        if (weather) {
            let background;
            Object.keys(backgrounds).forEach(el => {
                if (weather.includes(el)) {
                    background = backgrounds[el];
                }
            });
            if (!background) {
                background = backgrounds['base'];
            }
            return background;
        }
        return null;
    }
    constructor(
        private http: HttpClient,
        private geolocation: Geolocation
    ) {
        this.APPID = 'b735731f76b664f1ac760b6e8ed052ac';
        this.myGeo = this.getMyGeo();
        this.myFavoris = this.getMyFavorisList();
        this.updateMyFavoris();
        this.updateMyGeo();
    }
}
export class WeatherClass {
    constructor(
        public city: City,
        public list: List[]
    ) { }
}
export class List {
    constructor(
        public main: Main,
        public weather: Weather[],
        public wind: Wind,
        public dt_txt: Date
    ) { }
}
class Wind {
    constructor(
        public deg: number,
        public speed: number
    ) { }
}
class Weather {
    constructor(
        public description: string,
        public icon: string,
        public main: string,
        public id: number
    ) { }
}
class Main {
    constructor(
        public temp: number,
        public temp_max: number,
        public temp_min: number
    ) { }
}
class City {
    constructor(
        public coord: Coord,
        public country: string,
        public id: number,
        public name: string
    ) { }
}
class Coord {
    constructor(
        public lat,
        public lon
    ) { }
}
export const backgrounds = {
    base: './assets/imgs/base.gif',
    "Snow": './assets/imgs/snow.gif',
    "Mist": './assets/imgs/mist.gif',
    "Thunderstorm": './assets/imgs/thunderstorm.gif',
    "Rain": './assets/imgs/rain.gif',
    "Sun": './assets/imgs/sun.gif',
    "Clouds": './assets/imgs/clouds.gif',
    "Clear": './assets/imgs/base.gif',
}
