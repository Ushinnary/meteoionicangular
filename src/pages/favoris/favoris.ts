import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { WeatherClass, WeatherService } from '../../Services/weather.service';

@IonicPage({
    name: 'favoris',
    defaultHistory: ['home']
})
@Component({
    selector: 'page-favoris',
    templateUrl: 'favoris.html',
})
export class FavorisPage {
    ville = '';
    cityFound: WeatherClass;
    constructor(public navCtrl: NavController, public navParams: NavParams, protected ws: WeatherService) {
    }
    getVille() {
        this.cityFound = null;
        this.ws.getByCity(this.ville).subscribe(value => {
            this.cityFound = value;
        }, err => console.log(err.error.message));
    }
    addToFavoris() {
        if (!this.ws.myFavoris.find(el => el.city.name === this.cityFound.city.name)) {
            this.ws.favorisListChange(this.cityFound, true);
            this.ville = '';
            this.cityFound = null;
        } else {
            this.ville = '';
            this.cityFound = null;
        }
    }
    ionViewDidLoad() { }

}
