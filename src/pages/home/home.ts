import { Component } from '@angular/core';
import { NavController, IonicPage } from 'ionic-angular';
import { WeatherService, WeatherClass } from '../../Services/weather.service';

@IonicPage({
    name: 'home'
})
@Component({
    selector: 'page-home',
    templateUrl: 'home.html'
})
export class HomePage {
    mainWeather: string;
    constructor(
        public navCtrl: NavController,
        protected weatherService: WeatherService
    ) { }
    addFavoris() {
        this.navCtrl.push('favoris');
    }
    setBg(position: WeatherClass) {
        this.mainWeather = position.list[0].weather[0].main;
    }
    navigation(value: WeatherClass) {
        this.navCtrl.push('details', {
            'city': value['city']['name']
        });
    }
    update() {
        if (this.weatherService.myGeo) {
            this.mainWeather=this.weatherService.myGeo.list[0].weather[0].main;
        }
    }
    ionViewWillEnter() {
        this.update();
    }
}
