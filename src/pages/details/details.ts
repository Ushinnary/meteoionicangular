import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { WeatherService, WeatherClass, List } from '../../Services/weather.service';

@IonicPage({
    name: 'details',
    segment: 'details/:city',
    defaultHistory: ['home']
})
@Component({
    selector: 'page-details',
    templateUrl: 'details.html',
})
export class DetailsPage {
    weather: WeatherClass;
    joursDeLaSemaine: DayOfWeek[] = [];
    days = {
        1: 'Lundi',
        2: 'Mardi',
        3: 'Mercredi',
        4: 'Jeudi',
        5: 'Vendredi',
        6: 'Samedi',
        0: 'Dimanche'
    }
    getWeekWeather(list: List[]) {
        list.sort((a, b) => new Date(a.dt_txt).getTime() - new Date(b.dt_txt).getTime());
        let tempArray = [];
        for (let i = 0; list[i]; i++) {
            const value = list[i];
            tempArray.push(value);
            const lastFromArray = tempArray[tempArray.length - 1];
            if (i <= list.length - 2) {
                const next = list[i + 1];
                if (new Date(next['dt_txt']).getDate() !== new Date(lastFromArray['dt_txt']).getDate()) {
                    this.joursDeLaSemaine.push(new DayOfWeek(
                        this.days[new Date(tempArray[0]['dt_txt']).getDay()],
                        Math.max(...tempArray
                            .map(val => val['main']['temp_max'])
                        ),
                        Math.min(...tempArray
                            .map(val => val['main']['temp_max'])
                        )
                    ));
                    tempArray = [];
                }
            }
        }
        this.joursDeLaSemaine[0].name = "Aujourd'hui";
        this.joursDeLaSemaine[1].name = "Demain";
    }
    constructor(
        public navCtrl: NavController,
        public navParams: NavParams,
        private weatherService: WeatherService
    ) {
        if (this.weatherService.getMyFavorisList()) {
            const items = this.weatherService.getMyFavorisList();
            this.weather = items.find(el => el.city.name.toLowerCase() === this.navParams.data['city'].toLowerCase());
            if (this.weather) {
                if (new Date(this.weather.list[0].dt_txt).getDate() == new Date().getDate()
                    && new Date(this.weather.list[0].dt_txt).getTime() > new Date().getTime()) {
                    this.getWeekWeather(this.weather.list);
                } else {
                    this.update();
                }
            } else {
                this.update();
            }
        } else {
            this.update();
        }
    }
    update() {
        this.weatherService.getByCity(this.navParams.data['city']).subscribe(value => {
            this.weather = value;
            this.getWeekWeather(value['list']);
        });
    }
    ionViewDidLoad() {
    }

}
class DayOfWeek {
    constructor(
        public name: string,
        public tempDayAvg: number,
        public tempNightAvg: number
    ) { }
}
